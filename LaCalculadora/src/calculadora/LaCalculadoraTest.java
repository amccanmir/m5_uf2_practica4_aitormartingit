package calculadora;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LaCalculadoraTest {

	@Test
	void testSuma() {
		int res = LaCalculadora.suma(10, 20);
		assertEquals(30, res, "La suma no es correcta.");
//		fail("Not yet implemented");
	}

	@Test
	void testResta() {
		int res = LaCalculadora.resta(30, 20);
		assertEquals(10, res, "La resta no es correcta.");
//		fail("Not yet implemented");
	}

	@Test
	void testMultiplicacio() {
		int res = LaCalculadora.multiplicacio(5, 10);
		assertEquals(50, res, "La multiplicaciˇ no es correcta.");
//		fail("Not yet implemented");
	}

	@Test
	void testDivisio() {
//		fail("Not yet implemented");
	}
	
	@Test
	void testDivideix() {
		int res = LaCalculadora.divideix(12, 0);
	}
	
	@Test
	public void testException() {
		int res;
		try {
			res = LaCalculadora.divideix(10, 0);
			fail("Fallo: Passa per aqui si no es llenša la execpcio ArithmeticException");
		} catch (ArithmeticException e){
			System.out.println("La prova funciona correctament");
		}
	}

}
